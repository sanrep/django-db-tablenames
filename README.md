django_db_tablenames
====================

Add prefix and/or suffix to database table names on project, app or model level.

Reason for the project
----------------------

1. Some (external) projects automatically use a database prefix for
   interaction with a database. This is particularly common in implementations
   of the Active Record pattern.

2. It is possible to define an explicit database table name in the Meta
   class on a model; however, this is not as easily accomplished when dealing
   with a third-party application. By providing a high-level interface for
   adding prefixes/suffixes there is a simple, consistent way to achieve this
   goal other than forking the code or ad-hoc monkey patching.

Installation
------------

1. Install using pip:

    pip install git+https://gitlab.com/gitsandi/django-db-tablenames.git

2. Add django_db_tablenames at the top of your INSTALLED_APPS list. It is
   recommended that django_db_prefix is the first listed application, but it
   is essential that it be loaded before the initialization of any model you
   expect to be modified.

Configuration
-------------

Three configuration options are allowed: global, per-app or per-model. In all
cases, the specified prefix will be prepended and/or suffix appended exactly as
provided -- no delimiter will be added.

In all cases, behavior is controlled by the `DB_PREFIX` and `DB_SUFFIX` settings.

Global Prefix/Suffix
====================

To add a common prefix/suffix to all models simply set `DB_PREFIX`/`'DB_SUFFIX`
in settings to the string that you want to be prepended/appended.

    DB_PREFIX = 'foo_'
    DB_SUFFIX = '_bar'

For example, for the model appname.models.Baz the default table would be:
`appname_baz`

By setting `DB_PREFIX` to `foo`, the table would be `foo_appname_baz`.
By setting `DB_SUFFIX` to `bar`, the table would be `appname_baz_bar`.
By setting both, the table would be `foo_appname_baz_bar`.

Per-Application and Per-Model Prefix
====================================

Both per-application and per-model prefixes/suffixes are controlled by the use
of a dictionary as the value of `DB_PREFIX`/`DB_SUFFIX`, where the dictionary
contains keys allowing the lookup of a given model or application.

When `DB_PREFIX`/`DB_SUFFIX` is a dictionary, `django-db-tablenames` will
perform the following searches to determine the prefix/suffix to use. First
match wins; if no matches are found, no prefix/suffix will be added.

1. `app_label.model_name` following the standard Django convention, where
   `model_name` is converted to lowercase before attempting to match.
2. `app_label` where `app_label` is the name of the application.
3. `None` allowing for a global default when using the dictionary specification
   format.

Example:

    DB_PREFIX = {
        "appname.baz": "prefix_for_baz_model_",
        "appname2": "prefix_for_all_appname2_models_",
        None: "default_prefix_"
    }
    DB_SUFFIX = {
        "appname.baz": "_suffix_for_baz_model",
        "appname3": "_suffix_for_all_appname3_models",
        None: "_default_suffix"
    }

To disable prefix/suffix on a model, set its class attribute
`use_db_prefix`/`use_db_suffix` to False (if not defined, default is True).
