from django.conf import settings
from django.db.models.signals import class_prepared, pre_init


def change_db_tablenames(sender, **kwargs):
    app_label = sender._meta.app_label.lower()
    sender_name = sender._meta.object_name.lower()
    full_name = app_label + "." + sender_name

    prefix = getattr(settings, 'DB_PREFIX', None)
    if isinstance(prefix, dict):
        if full_name in prefix:
            prefix = prefix[full_name]
        elif app_label in prefix:
            prefix = prefix[app_label]
        else:
            prefix = prefix.get(None, None)
    if prefix and getattr(sender, 'use_db_prefix', True) and not sender._meta.db_table.startswith(prefix):
        sender._meta.db_table = prefix + sender._meta.db_table

    suffix = getattr(settings, 'DB_SUFFIX', None)
    if isinstance(suffix, dict):
        if full_name in suffix:
            suffix = suffix[full_name]
        elif app_label in suffix:
            suffix = suffix[app_label]
        else:
            suffix = suffix.get(None, None)
    if suffix and getattr(sender, 'use_db_suffix', True) and not sender._meta.db_table.endswith(suffix):
        sender._meta.db_table = sender._meta.db_table + suffix

pre_init.connect(change_db_tablenames)
class_prepared.connect(change_db_tablenames)
